/**
 * Given an array of integers and a 'target' integer, 
 * create a function that returns an array of the indexes for 
 * two numbers that add up to the target number. 
No index shall be used more than once, and you can expect 
every input to have a single solution.
The answer can be returned in any order.
Example 1:
	Input: nums = [2,7,11,15], target = 9
	Output: [0,1]
	Explanation:
	Output: Because nums[0] + nums[1] == 9, we return [0, 1].

  Example 2:
Input: nums = [3,2,4], target = 6
Output: [1,2]
Example 3:
Input: nums = [3,3], target = 6
Output: [0,1]
Example 4:
Input: nums = [0,3,4,7,5], target = 8
Output: [1,4]
 */

function indexesSum(nums, target) {
  let impArray = nums.map((num, index) => ({ num: num, i: index }));
  let tmp = impArray.sort((a, b) => a.num - b.num);
  console.log(tmp);

  let first = 0;
  let second = 1;
  let found = false;

  while (!found && first < impArray.length - 1 && second < impArray.length) {
    const sum = impArray[first].num + impArray[second].num;
    console.log("sum", sum);
    if (sum === target) {
      found = true;
      return [impArray[first].i, impArray[second].i];
    }
    console.log("b", first, second);
    if (sum < target && second < impArray.length) {
      second++;
    } else if (sum > target) {
      first++;
    }
  }
}

console.log(indexesSum([0, 3, 4, 7, 5], 8));
