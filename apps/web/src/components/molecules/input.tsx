import { Typography, TextField, Button } from "@mui/material";

import { ChangeEventHandler, useState } from "react";

export interface IInputProps {
  text: string;
  variant: string;
  stl: { lineHeight?: string; color?: string };
  type: "text" | "phone" | "email";
}

const validate = (text: string, type: string) => {
  if (type === "email") {
    text.match(
      /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
    );
  } else if (type === "phone") {
    text.match(/^\d{3}-\d{3}-\d{4}$/);
  }

  return true;
};

export const InputField: React.FC<IInputProps> = ({
  text,
  variant,
  stl,
  type,
}) => {
  const [isEditing, setIsEditing] = useState(false);
  const [val, setVal] = useState(text);
  const [showError, setShowError] = useState(false);

  const handleChange = (event: any) => {
    const { value } = event.target;
    const isValid = validate(value, type);
    if (!isValid) {
      setShowError(false);
    } else {
      setShowError(true);
    }

    setVal(value);
  };

  return isEditing ? (
    <>
      <TextField value={val} onChange={handleChange} error={showError} />
      <Button disabled={showError} onClick={() => setIsEditing(false)}>
        Save
      </Button>
    </>
  ) : (
    <Typography {...stl} onClick={() => setIsEditing(true)}>
      {val}
    </Typography>
  );
};
