import { Box, Typography, Avatar } from "@mui/material";
import { SystemStyleObject, Theme } from "@mui/system";

import { Card } from "@components/atoms";
import { IContact } from "react-coding-interview-shared/models";
import { InputField } from "./input";

export interface IContactCardProps {
  person: IContact;
  sx?: SystemStyleObject<Theme>;
}

export const ContactCard: React.FC<IContactCardProps> = ({
  person: { name, email },
  sx,
}) => {
  return (
    <Card sx={sx}>
      <Box display="flex" flexDirection="column" alignItems="center">
        <Avatar />
        <Box textAlign="center" mt={2}>
          <InputField
            text={name}
            variant="subtitle1"
            stl={{ lineHeight: "1rem" }}
            type="text"
          />
          <InputField
            text={email}
            variant="caption"
            stl={{ color: "text.secondary" }}
            type="email"
          />
        </Box>
      </Box>
    </Card>
  );
};
